#variable "AWS_ACCESS_KEY" {}
#variable "AWS_SECRET_KEY" {}
#variable "AWS_REGION" {}

resource "aws_iam_role" "lambda_ec2_backup" {
  name = "lambda_ec2_backup"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda_ec2_backup_policy" {
  name = "lambda_ec2_backup_policy"
  role = aws_iam_role.lambda_ec2_backup.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:CreateSnapshot",
                "ec2:CreateTags",
                "ec2:DeleteSnapshot",
                "ec2:DescribeInstances",
                "ec2:DescribeSnapshots",
                "ec2:DescribeImages",
                "ec2:CreateImage",
                "ec2:DeregisterImage"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = "../ec2-backup/"
  output_path = "ec2-backup.zip"
}

resource "aws_lambda_function" "lambda_ec2_backup" {
  filename         = "ec2-backup.zip"
  function_name    = "ec2-backup"
  role             = aws_iam_role.lambda_ec2_backup.arn
  handler          = "handler.lambda_handler"
  source_code_hash = data.archive_file.lambda_zip.output_base64sha256
  runtime          = "python3.6"
  memory_size      = 256
  timeout          = 300

  environment {
    variables = {
      DEFAULT_RETENTION_TIME = "14"
      LIMIT_TO_REGIONS       = "us-east-1,sa-east-1"
      DRY_RUN                = "false"
      KEY_TO_TAG_ON          = "Backup"
    }
  }
}

resource "aws_cloudwatch_event_rule" "lambda_ec2_backup" {
  name                = "lambda_ec2_backup"
  description         = "Run backups once a day"
  schedule_expression = "rate(1 day)"
}

resource "aws_cloudwatch_event_target" "lambda_ec2_backup" {
  rule      = aws_cloudwatch_event_rule.lambda_ec2_backup.name
  target_id = "ec2-backup"
  arn       = aws_lambda_function.lambda_ec2_backup.arn
}

resource "aws_lambda_permission" "lambda_ec2_backup" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_ec2_backup.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.lambda_ec2_backup.arn
}
